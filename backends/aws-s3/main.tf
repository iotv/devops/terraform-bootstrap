# Organization name
variable "org_name" {
  description = "The name of your organization to use for this terraform module"
  type        = "string"
}

# PGP Key
variable "pgp_key" {
  description = "The PGP public key to use when encrypting the secret back"
  type        = "string"
}

# DynamoDB table for locking the terraform state
resource "aws_dynamodb_table" "terraform_lock" {
  hash_key       = "LockID"
  name           = "${var.org_name}-terraform-locks"
  read_capacity  = 1
  write_capacity = 1

  attribute {
    name = "LockID"
    type = "S"
  }

  tags {
    Application = "Bootstrap"
    Environment = "Bastion"
    Name        = "${var.org_name} Terraform Lock Table"
    Stack       = "${var.org_name}-terraform-bootstrap"
  }
}

# IAM Access key for the terraform user
resource "aws_iam_access_key" "terraform" {
  pgp_key = "${var.pgp_key}"
  user    = "${aws_iam_user.terraform.name}"
}

# IAM user for terraform to use from here on out
resource "aws_iam_user" "terraform" {
  force_destroy = true
  name          = "${var.org_name}-terraform"
  path          = "/bastion/"
}

# Administrator policy for terraform
resource "aws_iam_user_policy_attachment" "terraform_administrator_access" {
  policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
  user       = "${aws_iam_user.terraform.name}"
}

# Terraform S3 Buckets KMS key
# This global KMS key is used for encrypting and decrypting s3 buckets used to
# store state for terraform
resource "aws_kms_key" "terraform_s3" {
  description         = "Key for terraform S3 serverside encryption"
  enable_key_rotation = true

  tags {
    Application = "Bootstrap"
    Environment = "Bastion"
    Name        = "${var.org_name} Terraform S3 Encryption Key"
    Stack       = "${var.org_name}-terraform-bootstrap"
  }
}

# Terraform S3 Buckets KMS alias
# This gives the KMS key a visible alias in the AWS console
resource "aws_kms_alias" "terraform_s3" {
  name_prefix   = "alias/${var.org_name}-terraform-s3"
  target_key_id = "${aws_kms_key.terraform_s3.key_id}"
}

# Terraform Log Bucket
# This global s3 bucket is designed to hold logs for the terraform bucket actions
resource "aws_s3_bucket" "terraform_logs" {
  bucket_prefix = "${var.org_name}-tf-logs"
  acl           = "log-delivery-write"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = "${aws_kms_key.terraform_s3.arn}"
        sse_algorithm     = "aws:kms"
      }
    }
  }

  tags {
    Application = "Bootstrap"
    Environment = "Bastion"
    Name        = "${var.org_name} Terraform Log Bucket"
    Stack       = "${var.org_name}-terraform-bootstrap"
  }
}

# Terraform Bucket
# This global s3 bucket is designed to hold terraform shared files for the organization
# This is part of bootstrap because the bucket will be used to hold environment tfstate files,
# results of packer builds, and other shared resources used in provisioning
resource "aws_s3_bucket" "terraform" {
  bucket_prefix = "${var.org_name}-tf"
  acl           = "private"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = "${aws_kms_key.terraform_s3.arn}"
        sse_algorithm     = "aws:kms"
      }
    }
  }

  logging {
    target_bucket = "${aws_s3_bucket.terraform_logs.id}"
    target_prefix = "log/"
  }

  versioning {
    enabled = true
  }

  tags {
    Application = "Bootstrap"
    Environment = "Bastion"
    Name        = "${var.org_name} Terraform Bucket"
    Stack       = "${var.org_name}-terraform-bootstrap"
  }
}

# Output the DynamoDB table name for use in the backend script
output "dynamodb_table" {
  description = "The DynamoDB table name used for locking the terraform state file"
  sensitive   = true
  value       = "${aws_dynamodb_table.terraform_lock.id}"
}

# Output the KMS key arn for use in the backend script
output "kms_key_arn" {
  description = "The KMS Key ARN used for encypting terraform state in the bucket"
  sensitive   = true
  value       = "${aws_kms_key.terraform_s3.arn}"
}

# Output the S3 bucket name for use in the backend script
output "s3_bucket" {
  description = "The S3 bucket used for terraform state"
  sensitive   = true
  value       = "${aws_s3_bucket.terraform.id}"
}

# Output the secret key and access key for second run use
output "key_id" {
  description = "The key ID used for the terraform user"
  sensitive   = true
  value       = "${aws_iam_access_key.terraform.id}"
}

output "encrypted_secret" {
  description = "The secret key of the access key, encrypted against the PGP key provided; decrypt with `| base64 --decode | keybase pgp decrypt`"
  sensitive   = true
  value       = "${aws_iam_access_key.terraform.encrypted_secret}"
}
