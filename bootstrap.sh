#!/bin/bash

# Bash strict mode
set -euvo pipefail

AWS_DEFAULT_REGION=us-east-1
terraform init bootstrap/aws-s3
terraform apply bootstrap/aws-s3

AWS_ACCESS_KEY_ID=$(terraform output -module=aws key_id)
AWS_SECRET_ACCESS_KEY=$(terraform output -module=aws encrypted_secret | base64 --decode | keybase pgp decrypt)
S3_BUCKET=$(terraform output -module=aws s3_bucket)
KMS_KEY_ARN=$(terraform output -module=aws kms_key_arn)
DYNAMODB_TABLE=$(terraform output -module=aws dynamodb_table)

terraform init \
    -backend-config="bucket=${S3_BUCKET}" \
    -backend-config="key=bastion/bootstrap.tfstate" \
    -backend-config="encrypt=true" \
    -backend-config="kms_key_id=${KMS_KEY_ARN}" \
    -backend-config="dynamodb_table=${DYNAMODB_TABLE}" \
    backend
