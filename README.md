[![pipeline status](https://gitlab.com/iotv/devops/terraform-bootstrap/badges/master/pipeline.svg)](https://gitlab.com/iotv/devops/terraform-bootstrap/commits/master)

# Terraform Bootstrap

The purpose of this project is to setup a working terraform/gitlab-ci connection for use within other projects in the iotv/devops group on gitlab.
This project functions differently from other terraform projects in the iotv/devops; namely:

* It only utilizes one environment -- Bastion.
  This environment is where meta-apps related to operating cloud infrastructure at an omnipotent level go.
  For now, that's just terraform.
* It has 2 seperate plans and runs.
  1. The "bootstrap run".
     This is the run where it provisions all of the things it needs to save its own state, for example, an s3 bucket and dynamodb table for managing a remote tfstate file.
     This run is **only performed on the first run.** The first run was manual.
  2. The "backend shared provision run".
     This is the run where it builds resources needed for orchestrating our cloud backend in other terraform projects.
     These resources, for example, admin users, have a saved state which is stored in the previously provisioned buckets.
     This run is **performed on every subsequent run.** All subsequent runs are in gitlab-ci.
* The layout is unusual because of it's self-hosting.
* Some of the variables are populated from **after** the first bootstrap run was performed, and recovery will require a user to manually run bootstrap then populate these fields.