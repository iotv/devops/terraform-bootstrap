provider "aws" {
  alias = "default"
}

module "backend" {
  source = "../../backends/aws-s3"

  org_name = "iotv"
  pgp_key  = "keybase:davidjfelix"

  providers = {
    aws = "aws.default"
  }
}
